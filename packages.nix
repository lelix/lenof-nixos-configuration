{config, lib, pkgs, ...}: {
  fonts.fonts = with pkgs; [
    font-awesome
    powerline-fonts
  ];

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    # Basic utilities
    ##################

    file
    fortune
    gnupg
    htop
    lesspipe
    nix-index
    openssl
    ripgrep
    tree
    unzip
    wget

    # Extra facilities

    brightnessctl
    samba

    # Common applications
    ######################

    emacs

    # Development
    ##############

    gcc
    git
    git-crypt
    gnumake
    python3
    tmux
  ];

  users.users.lele.packages = with pkgs; [
    # Basic utilities
    ##################

    aspell
    aspellDicts.en
    aspellDicts.it
    audio-recorder
    imagemagick
    ripgrep
#    w3m
    zip

    # Mail related
    ###############

    goimapnotify
    isync
    lieer
    msmtp
    notmuch
    pass

    # X11 stuff
    ############

    arandr
    i3status-rust
    networkmanagerapplet
    pango
    pasystray
    pavucontrol
    unclutter-xfixes
    xorg.xbacklight

    # Common applications
    ######################

    chromium
    claws-mail
#    dropbox-cli
    # As of Dec 10, 2021 the emacsGcc points to 28.0.90, but I want 29!
    # See https://github.com/nix-community/emacs-overlay/commit/482974075fe91803c4a6a157975e65198dc0a50e
    emacsGit
#    emacsGcc
    #emacsPgtkGcc
    evince
    firefox
    gimp
    gnome3.cheese
    gvfs
#    joplin
    libreoffice
    pqiv
    remmina
    sakura
    signal-desktop
#    thunar
    vlc
#    xbmc
    zoom-us

    # Development
    ##############

    gettext
    libxml2
    sqlite
    tmux
  ];
}
