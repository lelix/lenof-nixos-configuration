# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, options, ... }:

{
  imports = [
    # Include the results of the hardware scan
    ./hardware-configuration.nix

    # Enable and configure TLP service, controlling battery life
    ./tlp.nix

    # Configure users
    ./users.nix

    # Global and users packages
    ./packages.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Use latest linux kernel.
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [
    "mem_sleep_default=deep"
    # see https://github.com/NixOS/nixpkgs/pull/102106
    # and https://github.com/erpalma/throttled/issues/215
    # This should not be needed anymore, the PR above has been merged
    # "msr.allow_writes=on"
  ];

  # Cleanup /tmp at boot.
  boot.cleanTmpDir = true;

  # # Coerce the Apple keyboard to saner layout, see https://wiki.archlinux.org/index.php/Apple_Keyboard
  # boot.extraModprobeConfig = ''
  #   # fnmode=2       :: normally function keys, switchable to media keys by holding Fn key
  #   # swap_opt_cmd=1 :: swap the Option ("Alt") and Command ("Flag") keys
  #   options hid_apple fnmode=2 swap_opt_cmd=1
  # '';

  networking = {
    hostName = "lenof"; # Define your hostname.
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    firewall.enable = false;
    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.enp0s31f6.useDHCP = true;
    interfaces.wlp0s20f3.useDHCP = true;

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
    networkmanager = {
      enable = true;
      enableStrongSwan = true;
      wifi = {
        powersave = true;
      };
    };
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    extraHosts = ''
# Home hosts
192.168.1.1 tp-link.nautilus tp-link
192.168.1.100 nautilus.nautilus
'';
  };

  # Select internationalisation properties.
  i18n = {
      defaultLocale = "it_IT.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Rome";

  console = {
      keyMap = "it";
      packages = [ pkgs.terminus_font ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  # enable bonjour
  services.avahi = {
    enable = true;
    nssmdns = true;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable Gnome Virtual FS
  services.gvfs.enable = true;

  # Enable sound.
  sound.enable = true;
  sound.mediaKeys.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "it";
  services.xserver.xkbOptions = "eurosign:e";
  hardware.opengl.enable = true;
  hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];
  hardware.video.hidpi.enable = true;
  
  # Enable backlight support
  # programs.light.enable = true;
  services.actkbd = {
    enable = true;
    bindings = [
      # Brightness
      { keys = [ 224 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/brightnessctl set 10%-"; }
      { keys = [ 225 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/brightnessctl set 10%+"; }
    ];
  };
  
  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Enable Mate.
  # services.xserver.desktopManager.mate.enable = true;

  # Enable i3.
  services.xserver.windowManager.i3 = {
    enable = true;
    extraSessionCommands = ''
  
nm-applet &
EDITOR=emacsclient
DPI=210
TERMINAL=sakura
XCURSOR_SIZE=48
export EDITOR TERMINAL XCURSOR_SIZE DPI
echo Xft.dpi: $DPI | xrdb -merge
  '';
    extraPackages = with pkgs; [
      dmenu
      i3status
      i3lock
      (python3Packages.py3status.overrideAttrs (oldAttrs: {
        propagatedBuildInputs = [
          python3Packages.pytz
          python3Packages.tzlocal
        ];
      }))
    ];
  };

  services.xserver.displayManager.lightdm.greeters.gtk.cursorTheme = {
    name = "Vanilla-DMZ";
    package = pkgs.vanilla-dmz;
    size = 48;
  };

  # Firma digitale.
  services.pcscd.enable = true;
  services.pcscd.plugins = [pkgs.ccid pkgs.libacr38u];
  services.fwupd.enable = true;

  # Enable periodic fstrim.
  services.fstrim.enable = true;

  virtualisation = {
    docker.enable = true;
    virtualbox.host.enable = true;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

  # Pin the version systemwide
  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    trustedUsers = [ "root" "lele" ];
  };
}
