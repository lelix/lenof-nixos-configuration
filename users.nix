{config, lib, pkgs, ...}: {

  users.users.lele = {
    description = "Lele Gaifax";
    isNormalUser = true;
    extraGroups = [
      "adm"     # Mainly to work inside /etc/nixos/
      "docker"
      "vboxusers"
      "video"   # For brightnessctl
      "wheel"   # Enable ‘sudo’ for the user.
    ];
  };
}
