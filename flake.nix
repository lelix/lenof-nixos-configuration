{
  inputs = {
    emacs.url = "github:nix-community/emacs-overlay";
    nixos-hw.url = "github:NixOS/nixos-hardware";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = { self, emacs, nixos-hw, nixpkgs }: {

    nixosConfigurations.lenof = nixpkgs.lib.nixosSystem rec {
      system = "x86_64-linux";
      modules =
        [ ./configuration.nix
          # Lenovo X1 hardware tweaks
          #   <nixos-hardware/lenovo/thinkpad/x1/6th-gen/QHD/default.nix>
          #   <nixos-hardware/lenovo/thinkpad/x1/7th-gen/default.nix>
          nixos-hw.nixosModules.lenovo-thinkpad-x1-7th-gen
          nixpkgs.nixosModules.notDetected
          ({ pkgs, ... }: {
            nix = {
              registry = {
                nixpkgs.flake = nixpkgs;
                nixos-hardware.flake = nixos-hw;
              };
              binaryCaches = [
                "https://nix-community.cachix.org/"
              ];
              binaryCachePublicKeys = [
                "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
              ];
            };
            nixpkgs.overlays = [
              emacs.overlay
            ];
          })
        ];
    };
  };
}
